n = input()
string = raw_input()
cockroach_list = []
index = 0
cockroach_list.insert(0, 'nil')
for i in string:
	cockroach_list.append(i)
count = {'ro' : 0, 're': 0, 'bo' : 0, 'be' : 0}
maxnum = 0
for index, i in enumerate(cockroach_list):
	if index == 0:
		continue
	if i == 'r':
		if index % 2 != 0:
			count['ro'] = count['ro'] + 1
			if count['ro'] > maxnum:
				maxnum = count['ro']
				maxid = 'ro'
		else:
			count['re'] = count['re'] + 1
	else:
		if index % 2 != 0:
			count['bo'] = count['bo'] + 1
			if count['bo'] > maxnum:
				maxnum = count['bo']
				maxid = 'bo'
		else:
			count['be'] = count['be'] + 1
if maxid == 'ro':
	answer = min(count['re'], count['bo']) + abs(count['re'] - count['bo'])
else:
	answer = min(count['be'], count['ro']) + abs(count['be'] - count['ro'])
print answer


