number_of_days, minimum_walks = raw_input().split(' ')
number_of_days = int(number_of_days)
minimum_walks = int(minimum_walks)
day_walks = [int(i) for i in raw_input().split(' ')]
minimum_additions = 0
n = len(day_walks)
for i in range(1, n, 2):
	if i > (n - 1):
		break
	else:
		a = day_walks[i] + day_walks[i - 1]
		if i != (n - 1):
			b = day_walks[i] + day_walks[i + 1]
			addition = max(max(0, minimum_walks - a), max(0,minimum_walks - b))
			minimum_additions = minimum_additions + addition
			day_walks[i] = day_walks[i] + addition
		else:
			addition = max(0, minimum_walks - a)
			minimum_additions = minimum_additions + addition 
			day_walks[i] = day_walks[i] + addition
print minimum_additions
print day_walks

