number_of_elements = input()
elements = [int(i) for i in raw_input().split(' ')]
for i in reversed(elements):
		if i == 15:
			print "DOWN"
			exit()
		elif i == 0:
			print "UP"	
			exit()

diff = elements[len(elements) - 1] - elements[0]
if diff > 0:
	print "UP"
elif diff == 0:
	print "-1"
else:
	print "DOWN"