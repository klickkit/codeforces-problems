number_of_words, computer_delay = raw_input().split(' ')
number_of_words = int(number_of_words)
computer_delay = int(computer_delay)
type_time_sequence = [int(i) for i in raw_input().split(' ')]
words_on_display = 1
for index, i in enumerate(type_time_sequence):
	if index == 0:
		continue
	else:
		if (i - type_time_sequence[index - 1]) > computer_delay:
			words_on_display = 1
		else:
			words_on_display = words_on_display + 1
print words_on_display


