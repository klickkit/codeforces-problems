n, x0 = raw_input().split(' ')
n = int(n)
x0 = int(x0)
segment_boundary_list = []
for i in range(n):
	start, end = raw_input().split()
	start = int(start)
	end = int(end)
	segment_boundary_list.append(min(start, end))
common_point = max(segment_boundary_list)
movement = common_point - x0
print abs(movement)