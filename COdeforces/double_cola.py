import math
def search(n):
    global list
    left, right = 0, len(list) - 1
    while left <= right:
        mid = (left + right) / 2
        if list[mid][0] == n:
            return list[mid]
        elif n > list[mid][0]:
            left = mid + 1
            if n < list[left][0]:
                return list[mid]
        else:
            right = mid - 1
            if n > list[right][0]:
                return list[right]
n = input()
names = ['Sheldon', 'Leonard', 'Penny', 'Rajesh', 'Howard']
list = [0]
temp = 0
index = 0
while(1):
	multiplier = int(math.pow(2, index))
	temp = temp + (5 * multiplier)
	list.append((temp, multiplier))
	index = index + 1
	if  temp > n:
		break
nearest_value = search(n)
difference = (n - nearest_value[0])
divider = nearest_value[1] * 2
quotient = (difference / divider)
remainder = difference % divider
if remainder != 0:
	quotient = quotient + 1
answer = names[quotient - 1]
print answer
