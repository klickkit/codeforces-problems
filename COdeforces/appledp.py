import sys
n, k = raw_input().split(' ')
n = int(n)
k = int(k)
cost_for_packets = [int(i) for i in raw_input().split(' ')]
optimal_cost = [0 for i in range(k + 1)]
if cost_for_packets[1] == -1:
	cost_for_packets[1] = sys.maxint
optimal_cost[1] = cost_for_packets[0] 
for index, i in enumerate(cost_for_packets):
	if index == 0:
		continue
	else:
		broken_down_cost = optimal_cost[index] + optimal_cost[(index + 1) - index]
		print broken_down_cost
		optimal_cost[index + 1] = min(i, broken_down_cost)
		if optimal_cost[index] > sys.maxint:
			optimal_cost[index] = sys.maxint
print optimal_cost






