'''

d — the distance from home to the post office;
k — the distance, which car is able to drive before breaking;
a — the time, which Vasiliy spends to drive 1 kilometer on his car;
b — the time, which Vasiliy spends to walk 1 kilometer on foot;
t — the time, which Vasiliy spends to repair his car.
'''
d, k, a, b, t = raw_input().split(' ')
d = int(d)
k = int(k)
a = int(a)
b = int(b)
t = int(t)

def find_min_distance(n):
	global d, k, a, b, t
	if n == 0:
		return 0 
	if n % k != 1:
		distance = min(a + find_min_distance(), b + find_min_distance)
		return distance
	else
		distance = min(t + a + find_min_distance(), b + find_min_distance)
	return distance

n = input("Enter the number of kilometres to travel")
distance = find_min_distance(n)
print distance

