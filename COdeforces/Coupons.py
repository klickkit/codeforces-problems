number_of_days = input()
pizzas_required_on_days = [int(i) for i in raw_input().split(' ')]
def calcAnswer():
	sum = 0
	for i in pizzas_required_on_days:
		if i == 0 and sum % 2 != 0:
			return "NO"
		else:
			sum = sum + i
	if sum % 2 == 0:
		print "YES"
	else:
		print "NO"

result = calcAnswer()
if result:
	print result