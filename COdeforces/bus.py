bus_frequency_at_a, travel_time_for_a = raw_input().split(' ')
bus_frequency_at_b, travel_time_for_b = raw_input().split(' ')
bus_frequency_at_a = int(bus_frequency_at_a)
travel_time_for_a = int(travel_time_for_a) 
bus_frequency_at_b = int(bus_frequency_at_b)
travel_time_for_b = int(travel_time_for_b)
start_time_at_a = raw_input()
a = ( (start_time_at_a[0 : 2] * 60) + start_time_at_a[3 :]) 
diff = a - travel_time_for_b
time_hrs  = diff / 60
time_mins = diff % 60

