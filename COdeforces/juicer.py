number_of_oranges, height, threshold = raw_input().split(' ')
number_of_oranges = int(number_of_oranges)
height = int(height)
threshold = int(threshold)
orange_list = [int(i) for i in raw_input().split('  ')]
sum = 0
result = 0
for i in orange_list:
	if i > height:
		continue
	else:
		sum = sum + i
result = sum / threshold
print result