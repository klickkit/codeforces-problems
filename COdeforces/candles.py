a, b = raw_input().split(' ')
a = int(a)
b = int(b)
sum = a
def divide(number_of_candles):
	global sum, remainders
	if b > number_of_candles:
		remainders = remainders + number_of_candles
		return 0
	else:
		sum = sum + number_of_candles / b
		remainders = remainders + number_of_candles % b
		return divide(number_of_candles / b)
remainders = 0
divide(a)
while remainders >= b:
	temp = remainders
	remainders = 0
	divide(temp)
print sum


