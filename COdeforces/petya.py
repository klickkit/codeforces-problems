String1 = raw_input()
String2 = raw_input()
String1 = String1.lower()
String2 = String2.lower()
sum1 = 0
sum2 = 0
if String1 != String2:
    for i, j in zip(String1, String2):
        sum1 = sum1 + ord(i)
        sum2 = sum2 + ord(j)
    if sum1 < sum2:
        print '-1'
    elif sum1 > sum2:
        print '1'
    else:
        print '0'
else:
    print '0'
    